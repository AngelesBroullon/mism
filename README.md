# MISM

![EVO Milling machine](https://gitlab.com/AngelesBroullon/mism/raw/master/MISM/milling%20evo.jpg)

## Multimedia Information System for Milling

This work is focused on the generation of support systems for milling 
experiments creation and editing in CNC machining center. In this project, it
has been noticed the increasing need for generate, document and keep in the data
from experiments done in this machines, because of the it's needed repeat and
present them later as a document of the test for the results or to get
conclusions for divulge tasks.

It is also important to point that this work has been made under the free
software and information research use and distribution. It has been intend to
make a system that gives agility, clearly and easy information about the results
obtained, that can be already existent or new ones.

---

## Sistema multimedia de ayuda para corte y pulido superficial en maquinas CNC.

**Pregunta Motriz:** ¿Es posible realizar un sistema multimedia en codigo abierto para maquinas CNC?

**Datos de los autores:**

* Nombres: Mª Ángeles Broullón Lozano y Ana Mª Fernández Hernández.
* Titulación: Ingeniería Informática Superior.
* Asignatura: Sistemas Informaticos (Proyecto de fin de carrera).
* Tutor: Victor Flores Fonseca.
* Entidades relacionas: Instituto de Automática Industrial del CSIC.

**Palabras clave:** Multimedia, CNC, open source.

**Resumen:** El proyecto realizado comprende la creación de un sistema de ayuda
para los operadores de maquinas CNC (control numérico computarizado) del
Instituto de Automática Industrial del CSIC (Centro superior de investigaciones
científicas) que son utilizadas en pruebas para procesos industriales de pulido
y corte superficial.

Se trata de un ambicioso proyecto debido a que los autores pretendieron crear el
sistema completo, incluyendo imágenes en 3D de la pieza resultante del experimento y
una base de conocimiento que ayude a la hora de realizar la toma de decisiones y de
la predicción del resultado final del proceso, utilizando únicamente software libre
y con el objetivo claro de que fuese multiplataforma.

Para ello, se realizó todo el sistema utilizando librerías de java y el motor de
Prolog, que son potentes herramientas, las cuales permiten gran versatilidad a la hora
de trabajar y que permiten que el software final sea independiente del sistema operativo
en el que se ha creado. El proyecto al completo posee un objetivo claro cuya clave se
basa en la simpleza y la facilidad de utilización de los usuarios, ya que habitualmente
los operarios de este tipo de maquinas no poseen amplios conocimientos en informática.
Al mismo tiempo, se pretendía facilitarles la realización de experimentos, inclusión de
nuevas herramientas, sensores y demás personalizaciones que por lo general las soluciones
comerciales o el software perteneciente a los fabricantes no contemplan. Del mismo modo,
el interfaz del sistema permite cambiar el idioma de uso de ingles a castellano, siendo
multilingue.

El proyecto al completo fue realizado en inglés, lo que permite que el programa pueda
tener con mayor facilidad una salida para su uso en procesos industriales y del mismo
modo, sirviendo a los autores como entrenamiento en la practica de este idioma, en lo que
podría ser con facilidad un simulacro de situaciones que unos pocos meses después
deberían enfrentar en el mundo real.
